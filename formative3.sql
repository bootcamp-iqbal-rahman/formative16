SELECT
    p.id AS product_id,
    p.artnumber,
    p.name AS product_name,
    m.name AS manufacturer_name,
    b.name AS brand_name,
    a.street,
    a.city,
    a.province,
    a.country,
    a.zip_code,
    pr.amount,
    v.code AS valuta_code
FROM
    product p
JOIN
    manufacturer m ON p.manufacture_id = m.id
JOIN
    brand b ON p.brand_id = b.id
JOIN
    address a ON m.address_id = a.id
JOIN
    price pr ON p.id = pr.product_id
JOIN
    valuta v ON pr.valuta_id = v.id
WHERE
    p.stock = 0;