package id.co.nexsoft.stockmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.stockmanagement.model.Brand;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@RestController
@RequestMapping("api/brand")
public class BrandController {
    @Autowired
    private DefaultService<Brand> defaultService;

    @GetMapping
    public List<Brand> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Brand getDataById(@PathVariable int id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    public void saveData(@RequestBody Brand data) {
        defaultService.saveData(data);
    }

    @PostMapping("/{id}")
    public void updateData(@RequestBody Brand data, @PathVariable int id) {
        defaultService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        defaultService.deleteData(id);
    }
}
