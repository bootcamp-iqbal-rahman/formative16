package id.co.nexsoft.stockmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.stockmanagement.model.Address;
import id.co.nexsoft.stockmanagement.repository.AddressRepository;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@Service
public class AddressServiceImpl implements DefaultService<Address> {
    @Autowired
    private AddressRepository addressRepository;
    
    @Override
    public List<Address> getAllData() {
        return addressRepository.getAllData();
    }

    @Override
    public Address getDataById(int id) {
        return addressRepository.getDataById(id);
    }

    @Override
    public void saveData(Address data) {
        addressRepository.save(data);
    }

    @Override
    public void updateData(Address data, int id) {
        data.setId(id);
        addressRepository.save(data);
    }

    @Override
    public void deleteData(int id) {
        addressRepository.deleteById(id);
    }
    
}
