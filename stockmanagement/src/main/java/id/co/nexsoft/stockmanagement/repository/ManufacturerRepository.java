package id.co.nexsoft.stockmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.stockmanagement.model.Manufacturer;

public interface ManufacturerRepository extends CrudRepository<Manufacturer, Integer> {
    @Query("SELECT m FROM Manufacturer m")
    List<Manufacturer> getAllData();

    @Query("SELECT m FROM Manufacturer m WHERE m.id = :id")
    Manufacturer getDataById(int id);

    @Query("DELETE FROM Manufacturer m WHERE m.id = :id")
    void deleteById(int id);
}