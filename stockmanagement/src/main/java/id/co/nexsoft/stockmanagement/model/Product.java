package id.co.nexsoft.stockmanagement.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String artnumber;
    private String name;
    private String description;

    @ManyToOne
    @JoinColumn(name = "manufacture_id")
    @JsonManagedReference
    private Manufacturer manufacturer;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    private int stock;
}
