package id.co.nexsoft.stockmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.stockmanagement.model.Valuta;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@RestController
@RequestMapping("api/valuta")
public class ValutaController {
    @Autowired
    private DefaultService<Valuta> defaultService;

    @GetMapping
    public List<Valuta> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Valuta getDataById(@PathVariable int id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    public void saveData(@RequestBody Valuta data) {
        defaultService.saveData(data);
    }

    @PostMapping("/{id}")
    public void updateData(@RequestBody Valuta data, @PathVariable int id) {
        defaultService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        defaultService.deleteData(id);
    }
}
