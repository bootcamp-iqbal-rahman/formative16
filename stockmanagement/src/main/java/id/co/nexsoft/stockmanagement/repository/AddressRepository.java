package id.co.nexsoft.stockmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.stockmanagement.model.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {
    @Query("SELECT a FROM Address a")
    List<Address> getAllData();

    @Query("SELECT a FROM Address a WHERE a.id = :id")
    Address getDataById(int id);

    @Query("DELETE FROM Address a WHERE a.id = :id")
    void deleteById(int id);
}