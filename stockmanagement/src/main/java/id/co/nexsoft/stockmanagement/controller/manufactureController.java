package id.co.nexsoft.stockmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.stockmanagement.model.Manufacturer;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@RestController
@RequestMapping("api/manufacture")
public class manufactureController {
    @Autowired
    private DefaultService<Manufacturer> defaultService;

    @GetMapping
    public List<Manufacturer> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Manufacturer getDataById(@PathVariable int id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    public void saveData(@RequestBody Manufacturer data) {
        defaultService.saveData(data);
    }

    @PostMapping("/{id}")
    public void updateData(@RequestBody Manufacturer data, @PathVariable int id) {
        defaultService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        defaultService.deleteData(id);
    }
}
