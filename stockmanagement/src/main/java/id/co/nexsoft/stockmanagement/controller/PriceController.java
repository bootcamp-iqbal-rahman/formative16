package id.co.nexsoft.stockmanagement.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.stockmanagement.model.Price;
import id.co.nexsoft.stockmanagement.service.CustomService;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@RestController
@RequestMapping("api/price")
public class PriceController {
    @Autowired
    private DefaultService<Price> defaultService;

    @Autowired
    private CustomService<Price> customService;

    @GetMapping
    public List<Price> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Price getDataById(@PathVariable int id) {
        return defaultService.getDataById(id);
    }

    @GetMapping("/max/{max}")
    public List<Price> getDataMax(@PathVariable int max) {
        return customService.getDataMax(max);
    }

    @GetMapping("/twotable")
    public List<Price> getTwoJoin() {
        return customService.getTwoJoin();
    }

    @GetMapping("/join/valuta")
    public List<Map<String, Object>> getJoinTable() {
        return customService.getJoinTable();
    }

    @PostMapping
    public void saveData(@RequestBody Price data) {
        defaultService.saveData(data);
    }

    @PostMapping("/{id}")
    public void updateData(@RequestBody Price data, @PathVariable int id) {
        defaultService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        defaultService.deleteData(id);
    }
}
