package id.co.nexsoft.stockmanagement.service;

import java.util.List;
import java.util.Map;

public interface CustomService<T> {
    List<T> getDataMax(int max);
    List<T> getTwoJoin();
    List<Map<String, Object>> getJoinTable();
}
