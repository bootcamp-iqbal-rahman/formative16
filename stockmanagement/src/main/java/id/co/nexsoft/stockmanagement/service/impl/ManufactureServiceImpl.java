package id.co.nexsoft.stockmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.stockmanagement.model.Manufacturer;
import id.co.nexsoft.stockmanagement.repository.ManufacturerRepository;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@Service
public class ManufactureServiceImpl implements DefaultService<Manufacturer> {
    @Autowired
    private ManufacturerRepository manufactureRepository;
    
    @Override
    public List<Manufacturer> getAllData() {
        return manufactureRepository.getAllData();
    }

    @Override
    public Manufacturer getDataById(int id) {
        return manufactureRepository.getDataById(id);
    }

    @Override
    public void saveData(Manufacturer data) {
        manufactureRepository.save(data);
    }

    @Override
    public void updateData(Manufacturer data, int id) {
        data.setId(id);
        manufactureRepository.save(data);
    }

    @Override
    public void deleteData(int id) {
        manufactureRepository.deleteById(id);
    }
    
}
