package id.co.nexsoft.stockmanagement.service;

import java.util.List;

public interface DefaultService<T> {
    List<T> getAllData();
    T getDataById(int id);
    void saveData(T data);
    void updateData(T data, int id);
    void deleteData(int id);
}
