package id.co.nexsoft.stockmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.stockmanagement.model.Valuta;

public interface ValutaRepository extends CrudRepository<Valuta, Integer> {
    @Query("SELECT v FROM Valuta v")
    List<Valuta> getAllData();

    @Query("SELECT v FROM Valuta v WHERE v.id = :id")
    Valuta getDataById(int id);

    @Query("DELETE FROM Valuta v WHERE v.id = :id")
    void deleteById(int id);
}