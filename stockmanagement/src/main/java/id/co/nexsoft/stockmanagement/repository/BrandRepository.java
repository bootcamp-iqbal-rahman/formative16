package id.co.nexsoft.stockmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.stockmanagement.model.Brand;

public interface BrandRepository extends CrudRepository<Brand, Integer> {
    @Query("SELECT b FROM Brand b")
    List<Brand> getAllData();

    @Query("SELECT b FROM Brand b WHERE b.id = :id")
    Brand getDataById(int id);

    @Query("DELETE FROM Brand b WHERE b.id = :id")
    void deleteById(int id);
}