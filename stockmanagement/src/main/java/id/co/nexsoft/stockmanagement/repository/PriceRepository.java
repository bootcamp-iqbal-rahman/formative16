package id.co.nexsoft.stockmanagement.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.nexsoft.stockmanagement.model.Price;

public interface PriceRepository extends JpaRepository<Price, Integer> {
    @Query("SELECT p FROM Price p")
    List<Price> getAllData();

    @Query("SELECT p FROM Price p WHERE p.id = :id")
    Price getDataById(int id);

    @Query("DELETE FROM Price p WHERE p.id = :id")
    void deleteById(int id);

    @Query(value = "SELECT * FROM price p LIMIT ?1", nativeQuery = true)
    List<Price> getDataMax(int max);

    @Query(value = "SELECT p.id, p.product_id, p.valuta_id, p.amount FROM price p LEFT JOIN valuta v on p.valuta_id = v.id", nativeQuery = true)
    List<Price> getTwoTable();

    @Query(value = "SELECT * FROM price p LEFT JOIN valuta t on p.valuta_id = t.id", nativeQuery = true)
    List<Map<String, Object>> getJoinTable();
}