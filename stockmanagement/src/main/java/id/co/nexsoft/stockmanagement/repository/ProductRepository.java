package id.co.nexsoft.stockmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.stockmanagement.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    @Query("SELECT p FROM Product p")
    List<Product> getAllData();

    @Query("SELECT p FROM Product p WHERE p.id = :id")
    Product getDataById(int id);

    @Query("DELETE FROM Product p WHERE p.id = :id")
    void deleteById(int id);
}