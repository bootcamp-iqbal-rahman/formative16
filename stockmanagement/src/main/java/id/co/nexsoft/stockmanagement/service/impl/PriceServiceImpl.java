package id.co.nexsoft.stockmanagement.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.stockmanagement.model.Price;
import id.co.nexsoft.stockmanagement.repository.PriceRepository;
import id.co.nexsoft.stockmanagement.service.CustomService;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@Service
public class PriceServiceImpl implements DefaultService<Price>, CustomService<Price> {
    @Autowired
    private PriceRepository priceRepository;
    
    @Override
    public List<Price> getAllData() {
        return priceRepository.getAllData();
    }

    @Override
    public Price getDataById(int id) {
        return priceRepository.getDataById(id);
    }

    @Override
    public void saveData(Price data) {
        priceRepository.save(data);
    }

    @Override
    public void updateData(Price data, int id) {
        data.setId(id);
        priceRepository.save(data);
    }

    @Override
    public void deleteData(int id) {
        priceRepository.deleteById(id);
    }

    @Override
    public List<Price> getDataMax(int max) {
        return priceRepository.getDataMax(max);
    }

    @Override
    public List<Price> getTwoJoin() {
        return priceRepository.getTwoTable();
    }

    @Override
    public List<Map<String, Object>> getJoinTable() {
        return priceRepository.getJoinTable();
    }
    
}
