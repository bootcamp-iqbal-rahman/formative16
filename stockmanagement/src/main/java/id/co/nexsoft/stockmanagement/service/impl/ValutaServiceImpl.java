package id.co.nexsoft.stockmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.stockmanagement.model.Valuta;
import id.co.nexsoft.stockmanagement.repository.ValutaRepository;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@Service
public class ValutaServiceImpl implements DefaultService<Valuta> {
    @Autowired
    private ValutaRepository valutaRepository;
    
    @Override
    public List<Valuta> getAllData() {
        return valutaRepository.getAllData();
    }

    @Override
    public Valuta getDataById(int id) {
        return valutaRepository.getDataById(id);
    }

    @Override
    public void saveData(Valuta data) {
        valutaRepository.save(data);
    }

    @Override
    public void updateData(Valuta data, int id) {
        data.setId(id);
        valutaRepository.save(data);
    }

    @Override
    public void deleteData(int id) {
        valutaRepository.deleteById(id);
    }
    
}
