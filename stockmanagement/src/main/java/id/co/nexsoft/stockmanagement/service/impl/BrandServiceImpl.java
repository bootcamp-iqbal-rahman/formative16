package id.co.nexsoft.stockmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.stockmanagement.model.Brand;
import id.co.nexsoft.stockmanagement.repository.BrandRepository;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@Service
public class BrandServiceImpl implements DefaultService<Brand> {
    @Autowired
    private BrandRepository brandRepository;
    
    @Override
    public List<Brand> getAllData() {
        return brandRepository.getAllData();
    }

    @Override
    public Brand getDataById(int id) {
        return brandRepository.getDataById(id);
    }

    @Override
    public void saveData(Brand data) {
        brandRepository.save(data);
    }

    @Override
    public void updateData(Brand data, int id) {
        data.setId(id);
        brandRepository.save(data);
    }

    @Override
    public void deleteData(int id) {
        brandRepository.deleteById(id);
    }
    
}
