package id.co.nexsoft.stockmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.stockmanagement.model.Product;
import id.co.nexsoft.stockmanagement.repository.ProductRepository;
import id.co.nexsoft.stockmanagement.service.DefaultService;

@Service
public class ProductServiceImpl implements DefaultService<Product> {
    @Autowired
    private ProductRepository productRepository;
    
    @Override
    public List<Product> getAllData() {
        return productRepository.getAllData();
    }

    @Override
    public Product getDataById(int id) {
        return productRepository.getDataById(id);
    }

    @Override
    public void saveData(Product data) {
        productRepository.save(data);
    }

    @Override
    public void updateData(Product data, int id) {
        data.setId(id);
        productRepository.save(data);
    }

    @Override
    public void deleteData(int id) {
        productRepository.deleteById(id);
    }
    
}
