SELECT
    v.code AS valuta_code,
    p.id AS product_id,
    p.artnumber,
    p.name AS product_name,
    m.name AS manufacturer_name,
    b.name AS brand_name,
    a.street,
    a.city,
    a.province,
    a.country,
    a.zip_code,
    MAX(pr.amount) AS max_price
FROM
    valuta v
JOIN
    price pr ON v.id = pr.valuta_id
JOIN
    product p ON pr.product_id = p.id
JOIN
    manufacturer m ON p.manufacture_id = m.id
JOIN
    brand b ON p.brand_id = b.id
JOIN
    address a ON m.address_id = a.id
GROUP BY
    v.code,
    p.id,
    p.artnumber,
    p.name,
    m.name,
    b.name,
    a.street,
    a.city,
    a.province,
    a.country,
    a.zip_code
ORDER BY
    max_price DESC;
