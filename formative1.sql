CREATE TABLE address (
  id int PRIMARY KEY AUTO_INCREMENT,
  street varchar(50),
  city varchar(50),
  province varchar(50),
  country varchar(50),
  zip_code varchar(50)
);

CREATE table brand (
  	id int AUTO_INCREMENT,
  	name varchar(255),
  	PRIMARY KEY (id)
);

CREATE TABLE manufacturer (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(100),
  address_id int,
  FOREIGN KEY (address_id) REFERENCES address(id)
);
  
CREATE TABLE product (
  	id int PRIMARY KEY AUTO_INCREMENT,
  	artnumber varchar(50),
  	name varchar(50),
  	description varchar(150),
  	manufacture_id int,
  	brand_id int,
  	stock int,
  	FOREIGN KEY (manufacture_id) REFERENCES manufacturer(id),
  	FOREIGN KEY (brand_id) REFERENCES brand(id)
);

CREATE TABLE valuta (
  id int PRIMARY KEY AUTO_INCREMENT,
  code varchar(10),
  name varchar(100)
);

CREATE TABLE price (
  id int PRIMARY KEY AUTO_INCREMENT,
  product_id int,
  valuta_id int,
  amount double,
  FOREIGN KEY (product_id) REFERENCES product(id),
  FOREIGN KEY (valuta_id) REFERENCES valuta(id)
);