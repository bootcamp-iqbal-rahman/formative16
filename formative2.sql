INSERT INTO brand 
	(name) 
VALUES
    ('Brand1'),
    ('Brand2')
;

INSERT INTO address 
	(street, city, province, country, zip_code) 
VALUES
    ('123 Main St', 'City1', 'Province1', 'Country1', '12345'),
    ('456 Oak St', 'City2', 'Province2', 'Country2', '67890')
;

INSERT INTO manufacturer 
	(name, address_id) 
VALUES
    ('Manufacturer1', 1),
    ('Manufacturer2', 2)
;

INSERT INTO valuta 
	(code, name) 
VALUES
    ('USD', 'United States Dollar'),
    ('IDR', 'Indonesian Rupiah')
;

INSERT INTO product 
	(artnumber, name, description, manufacture_id, brand_id, stock)
VALUES
    ('A001', 'Product1', 'Description1', 1, 1, 10),
    ('A002', 'Product2', 'Description2', 2, 2, 5),
    ('A003', 'Product3', 'Description3', 1, 1, 8),
    ('A004', 'Product4', 'Description4', 1, 2, 15),
    ('A005', 'Product5', 'Description5', 2, 1, 0),
    ('A006', 'Product6', 'Description6', 2, 1, 12),
    ('A007', 'Product7', 'Description7', 1, 2, 18),
    ('A008', 'Product8', 'Description8', 2, 1, 25),
    ('A009', 'Product9', 'Description9', 2, 1, 0),
    ('A010', 'Product10', 'Description10', 1, 2, 20)
;

INSERT INTO price 
	(product_id, valuta_id, amount) 
VALUES
    (1, 1, 10.50), (1, 2, 150000),
    (2, 1, 25.75), (2, 2, 300000),
    (3, 1, 8.99), (3, 2, 120000),
    (4, 1, 18.25), (4, 2, 250000),
    (5, 1, 12.99), (5, 2, 180000),
    (6, 1, 30.50), (6, 2, 400000),
    (7, 1, 15.75), (7, 2, 220000),
    (8, 1, 22.99), (8, 2, 350000),
    (9, 1, 28.25), (9, 2, 480000),
    (10, 1, 20.99), (10, 2, 300000)
;